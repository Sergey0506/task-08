package com.epam.rd.java.basic.task8;

import java.util.Collections;

import com.epam.rd.java.basic.task8.comparators.FlowersComparatorByName;
import com.epam.rd.java.basic.task8.comparators.FlowersComparatorByOrigin;
import com.epam.rd.java.basic.task8.comparators.FlowersComparatorBySoil;
import com.epam.rd.java.basic.task8.controller.*;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		DOMController domController = new DOMController(xmlFileName);
	
		domController.parseXML(true);
		
		// sort (case 1)
		Collections.sort(domController.getFlowers(), new FlowersComparatorByName());
		// save
		String outputXmlFile = "output.dom.xml";
		domController.saveToXML(outputXmlFile);

		
		
		
		
		
		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		saxController.parseXML(true);
		// sort  (case 2)
		Collections.sort(saxController.getFlowers(), new FlowersComparatorBySoil());
		// PLACE YOUR CODE HERE
		
		// save
		outputXmlFile = "output.sax.xml";
		saxController.saveToXML(outputXmlFile);
		// PLACE YOUR CODE HERE
		
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		staxController.parseXML(true);
		// sort  (case 3)
		Collections.sort(staxController.getFlowers(), new FlowersComparatorByOrigin());
		// PLACE YOUR CODE HERE
		
		// save
		outputXmlFile = "output.stax.xml";
		// PLACE YOUR CODE HERE
		staxController.saveToXML(outputXmlFile);
	}

}
